-- Filename: autocmds.lua
-- Last Change: Mon, 15 Aug 2022 19:53:11
-- source: https://bit.ly/3x8oiNR
-- vim:set softtabstop=2 shiftwidth=2 tabstop=2 expandtab:

local augroups = {}

augroups.buf_write_pre = {
  mkdir_before_saving = {
    event = {"BufWritePre", "FileWritePre"},
    pattern = "*",
    command = [[ silent! call mkdir(expand("<afile>:p:h"), "p") ]],
  },
  clear_search_highlighting = {
    event = "InsertEnter",
    pattern = "*",
    callback = function()
      vim.opt_local.hlsearch = false
      vim.fn.clearmatches()
    end,
  },

  match_extra_spaces = {
    event = "InsertLeave",
    pattern = "*",
    callback = function()
      vim.cmd [[
      set nopaste
      highlight RedundantSpaces ctermbg=red guibg=red
      match RedundantSpaces /\s\+$/
      ]]
    end,
  },
}

augroups.misc = {

  Packer_Update = {
    event = "BufWritePost",
    pattern = "plugins.lua",
    callback = function()
      vim.cmd [[source <afile> | PackerSync]]
    end,
  },

  change_header = {
   event = "BufWritePre",
   pattern = "*",
   callback = function()
     require('core.utils').changeheader()
   end,
  },

  trim_extra_spaces_and_newlines = {
   event = "BufWritePre",
   pattern = "*",
   callback = function()
     require("core.utils").preserve('%s/\\s\\+$//ge')
   end,
  },

  unlist_terminal = {
    event = "TermOpen",
    pattern = "*",
    command = [[
    tnoremap <buffer> <Esc> <c-\><c-n>
    tnoremap <buffer> <leader>x <c-\><c-n>:bd!<cr>
    tnoremap <expr> <A-r> '<c-\><c-n>"'.nr2char(getchar()).'pi'
    startinsert
    nnoremap <buffer> <C-c> i<C-c>
    setlocal listchars= nonumber norelativenumber
    setlocal nobuflisted
    ]],
  },

  fix_commentstring = {
    event = "BufEnter",
    pattern = "*config,*rc,*conf,sxhkdrc,bspwmrc",
    callback = function()
      vim.bo.commentstring = "#%s"
      vim.cmd('set syntax=config')
    end,
  },

  reload_myvimrc = {
    event = "BufWritePre",
    pattern = "$MYVIMRC",
    callback = function()
      ReloadConfig()
    end,
  },
  reload_sxhkd  = {
    event = "BufWritePost",
    pattern = "sxhkdrc",
    command = [[!pkill -USR1 -x sxhkd]],
  },

  make_scripts_executable = { -- chmod +x scripts
    event = "BufWritePost",
    pattern = "*.sh,*.py,*.zsh",
    callback = function()
      local file = vim.fn.expand("%p")
      local status = require('core.utils').is_executable()
      if status ~= true then
        vim.fn.setfperm(file, "rwxr-x---")
      end
    end
  },

  update_xresources = {
    event = "BufWritePost",
    pattern = "~/.Xresources",
    command = [[!xrdb -merge ~/.Xresources]],
  },

  updated_xdefaults = {
    event = "BufWritePost",
    pattern = "~/.Xdefaults",
    command = [[!xrdb -merge ~/.Xdefaults]],
  },

  restore_cursor_position = {
    event = "BufRead",
    pattern = "*",
    command = [[call setpos(".", getpos("'\""))]],
  },

  lwindow_quickfix = {
    event = "QuickFixCmdPost",
    pattern = "l*",
    command = [[lwindow | wincmd j]],
  },

  cwindow_quickfix = {
    event = "QuickFixCmdPost",
    pattern = "[^l]*",
    command = [[cwindow | wincmd j]],
  },

  auto_working_directory = {
    event = "BufEnter",
    pattern = "*",
    callback = function()
      vim.cmd("silent! lcd %:p:h")
    end,
  },
}

augroups.yankpost = {

  save_cursor_position = {
    event = { "VimEnter", "CursorMoved" },
    pattern = "*",
    callback = function()
      cursor_pos = vim.fn.getpos('.')
    end,
  },

  highlight_yank = {
    event = "TextYankPost",
    pattern = "*",
    callback = function ()
      vim.highlight.on_yank{higroup="IncSearch", timeout=400, on_visual=true}
    end,
  },

  yank_restore_cursor = {
    event = "TextYankPost",
    pattern = "*",
    callback = function()
      local cursor = vim.fn.getpos('.')
      if vim.v.event.operator == 'y' then
        vim.fn.setpos('.', cursor_pos)
      end
    end,
  },
}

augroups.quit = {

  quit_with_q = {
    event = "FileType",
    pattern = {"checkhealth", "fugitive", "git*", "lspinfo" },
    callback = function ()
      vim.api.nvim_buf_set_keymap(0, "n", "q", "<cmd>close!<cr>", {noremap = true, silent = true})
      vim.bo.buflisted = false
    end
  },
}

augroups.lsp = {

  highlight_lsp = {
    event = "CursorHold",
    pattern = "*",
    callback = vim.lsp.buf.document_highlight,
    desc = "highlight lsp preferences",
  },
}

for group, commands in pairs(augroups) do
  local augroup = vim.api.nvim_create_augroup("AU_"..group, {clear = true})

  for _, opts in pairs(commands) do
    local event = opts.event
    opts.event = nil
    opts.group = augroup
    vim.api.nvim_create_autocmd(event, opts)
  end
end
