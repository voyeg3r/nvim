-- Filename: /home/sergio/.config/nvim/lua/plugins_setup/mason.lua
-- Last Change: Fri, 19 Aug 2022 21:32:16

local status, mason = pcall(require, "mason")
if (not status) then return end

local status2, lspconfig = pcall(require, "mason-lspconfig")
if (not status2) then return end

require("mason").setup()
require("mason-lspconfig").setup()

mason.setup({
  ui = {
    icons = {
      package_installed = "✓",
      package_pending = "➜",
      package_uninstalled = "✗"
    }
  }
})

lspconfig.setup {
  ensure_installed = { "sumneko_lua", "tailwindcss" },
}

