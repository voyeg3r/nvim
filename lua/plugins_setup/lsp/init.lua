-- Filename: init.lua
-- Last Change: Fri, 12 Aug 2022 17:54:35
-- vim: ts=2 sw=2 et:

require('plugins_setup.lsp.settings')
require('plugins_setup.lsp.diagnostic')

-- Pretty icons
vim.lsp.protocol.CompletionItemKind = {
  '  [text]',
  '[method]',
  '  [function]',
  '[constructor]',
  '[field]',
  '[variable]',
  '[class]',
  '[interface]',
  '[module]',
  '[property]',
  '[unit]',
  '[value]',
  '[enum]',
  '[key]',
  '  [snippet]',
  '[color]',
  '[file]',
  '[reference]',
  '  [folder]',
  '[enum member]',
  '[constant]',
  '[struct]',
  '[event]',
  '[operator]',
  '[type]',
}

