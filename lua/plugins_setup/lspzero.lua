-- File: /data/data/com.termux/files/home/.config/nvim/lua/plugins_setup/lspzero.lua
-- Last Change: Sat, 13 Aug 2022 22:13:29

require('mason.settings').set({
  ui = {
    border = 'rounded'
  }
})

local lsp = require('lsp-zero')
lsp.preset('recommended')

lsp.setup()

local lsp = require('lsp-zero')

lsp.ensure_installed({
  'html',
  'cssls',
  'tsserver',
  'sumneko_lua'
})

--lsp.preset('recommended')
lsp.set_preferences({
  suggest_lsp_servers = true,
  setup_servers_on_start = true,
  set_lsp_keymaps = true,
  configure_diagnostics = true,
  cmp_capabilities = true,
  -- manage_nvim_cmp = true,
  call_servers = 'mason',
  sign_icons = {
    error = '✘',
    warn = '▲',
    hint = '⚑',
    info = ''
  }
})

lsp.nvim_workspace()
lsp.setup()
