-- Filename: fterm.lua
-- Last Change: Wed, 06 Jul 2022 - 16:58

require'FTerm'.setup({
    border = 'single',
    dimensions  = {
        height = 0.9,
        width = 0.9,
    },
})

