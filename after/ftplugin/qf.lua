-- Filename: qf.lua
-- Last Change: Fri, 17 Jun 2022 - 13:54

vim.wo.relativenumber = false
vim.wo.signcolumn = 'no'
vim.wo.scrolloff = 0
vim.opt_local.spell = false
vim.opt_local.buflisted = false
vim.api.nvim_buf_set_keymap(0, "n", "q", "<cmd>close!<cr>", {noremap = true, silent = true})
