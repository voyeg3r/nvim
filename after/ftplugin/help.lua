-- Filename: /home/sergio/.config/nvim/after/ftplugin/help.lua
-- Last Change: Sat, 20 Aug 2022 08:45:19

local function map(mode, lhs, rhs, opts)
    local options = { noremap = true, silent = true , buffer = true}
    if opts then
        if opts['desc'] then
            opts['desc'] = 'help.lua: ' .. opts['desc']
        end
        options = vim.tbl_extend('force', options, opts)
    end
    vim.keymap.set(mode, lhs, rhs, options)
end

map("n", "q", "<cmd>close!<cr>", { desc = "close help file using q"})
map('', '<cr>', '<c-]>', { desc = "jump to tags using <cr>"})
map('', '<m-enter>', '<c-t>', { desc = "get back from tag jump"})

