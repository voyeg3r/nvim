-- Filename: markdown.lua
-- Last Change: Thu, 11 Aug 2022 11:07:48

vim.opt_local.number = false
vim.opt_local.relativenumber = false
vim.g['loaded_spellfile_plugin'] = 0
vim.opt_local.spell = true
vim.bo.spelllang = 'en_us'
vim.bo.textwidth = 80
vim.opt_local.wrap = true
vim.opt_local.suffixesadd:prepend('.md')

vim.cmd([[highlight MinhasNotas ctermbg=Yellow ctermfg=red guibg=Yellow guifg=red]])
vim.cmd([[match MinhasNotas /NOTE:/]])

vim.keymap.set('n', ']]', function()
    vim.fn.search("^#")
    vim.cmd('normal zz')
    require('core.utils').flash_cursorline()
end,
{ buffer = true, desc = 'Jump to the next Heading' })

vim.keymap.set('n', '[[', function()
    vim.fn.search("^#", "b")
    vim.cmd('normal zz')
    require('core.utils').flash_cursorline()
end,
{ buffer = true, desc = 'Jump to the previous Heading' })

-- TODO:
--vim.keymap.set('n', '+', function()
---- map to increase and decrease markdown headings
--end,
--)
