-- Filename: lua.lua
-- Last Change: Mon, 15 Aug 2022 19:17:52
-- reference: r/neovim/comments/pl0p5v/comment/hvn0kff/

-- local opt_local = vim.opt_local
-- vim.opt_local.includeexpr , _ = vim.v.fname:gsub('%.', '/')
vim.cmd [[ setlocal includeexpr=substitute(v:fname,'\\.','/','g') ]]
vim.opt_local.suffixesadd:prepend '.lua'
vim.opt_local.suffixesadd:prepend 'init.lua'
vim.opt_local.spell = false
vim.bo.path = vim.o.path .. ',' .. vim.fn.stdpath('config')..'/lua'

vim.bo.shiftwidth = 2
vim.bo.tabstop = 2
vim.bo.softtabstop = 2
vim.bo.textwidth = 78
vim.bo.expandtab = true
vim.bo.autoindent = true
