-- Filename: /home/sergio/.config/nvim/after/ftplugin/sh.lua
-- Last Change: Thu, 16 Jun 2022 - 12:38
-- references: https://www.reddit.com/r/neovim/comments/od82rk/comment/h43x7jc/

vim.opt_local.iskeyword:append("$")
vim.opt_local.keywordprg = ":Man"
