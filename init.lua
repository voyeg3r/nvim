-- File: ~/.config/nvim/init.lua
-- Termux nvim config
-- Last Change: Sat, 20 Aug 2022 08:33:32
-- vim:tabstop=2 nospell:

local ok, impatient = pcall(require, "impatient")
if not ok then
  print("impatient was not loaded")
end

require('core.options')
require('core.plugins')
require('core.mappings')
require('core.autocomds')

